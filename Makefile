# Use with 
# cp /home/jake/stuff/makefiles/c.Makefile Makefile

CFLAGS=-Wall -Werror -O3
LIBS=`curl-config --cflags --libs`

## Add default targets here
TARGETS=resolver

default: $(TARGETS)

clean:
	rm -f *.o $(TARGETS)

## Examples

#wandtest: wandtest.c stuff.c
#		cc $(CFLAGS) $(shell MagickWand-config --cflags)	$^ -o $@
#		cc $(CFLAGS) -L/usr/include/linux -ljoystick -c $^
#		cc $(CFLAGS) $(shell freetype-config --cflags --libs) -lm -c $^

#%.pdf: %.tex


## Add your stuff here


resolver: resolver.o common.o
	$(CC) $^ -o $@ $(LIBS)
	cp -vf $@ bbb-resolver-cgi/
