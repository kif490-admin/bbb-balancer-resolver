# BBB Balancer Resolver

Resolve a BBB Room + a Username to its actual URL using the Greenlight BBB Load Balancer.

Url: `https://bbb-resolver.asta.uni-goettingen.de/bbb-resolver-cgi/resolver`

## Room Setup (IMPORTANT!)

Every BBB Room needs the following Settings:

`Require moderator approval before joining` must be set to FALSE.

`Allow any user to start this meeting` must be set to TRUE.


## Request (GET)

Example: https://bbb-resolver.asta.uni-goettingen.de/bbb-resolver-cgi/resolver?room=jak-qnt-6bn-zp2&name=Jake%C3%A4%C3%B6%C3%BC%20asd

Arguments:
- `room` is the BBB Room. (3 (`exa-a1b-cd2`) or 4 (`exa-a1b-cd2-3ef`) sections)
- `name` is the username. ([percent encoded](https://datatracker.ietf.org/doc/html/rfc3986) (space must be `%20`)) (maxlen = 100 char)

## Response

Content-type: `application/json`

The Response Statuscode is always 200. Even if an error occured.

### Success

```json
{ "bbbjoinurl": "<join url>", "bbburl": "<bbb url>" }
```

`bbbjoinurl` can be used in an Iframe.

`bbburl` is the normal BBB Url.

Example:

```json
{
	"bbbjoinurl": "https://c14.meet.gwdg.de/bigbluebutton/api/join?createTime=1618367385222&fullName=Jake%C3%A4%C3%B6%C3%BC+asd&join_via_html5=true&meetingID=oev1w0wfy1qsnp45gofceyzhwgiz9eqhknlstuxk&password=vFOakZswYAFQ&userID=gl-guest-04617b627c9b0b34cde87c8d&checksum=f6bd6e1884bfa65bba4bbfd35bb945c4d68f082e",
	"bbburl": "https://meet.gwdg.de/b/jak-qnt-6bn-zp2"
}
```


### Error

```json
{ "error": "<error message>" }
```

Example:

```json
{ "error": "Didn't receive a redirect. Is the BBB Room setup correctly? Does it even exist?" }
```



## Requirements

`sudo apt install libcurl4-openssl-dev fcgiwrap`

Nginx config:
```
[...]
        location /bbb-resolver-cgi/ {        
                # Disable gzip (it makes scripts feel slower since they have to complete
                # before getting gzipped)
                gzip off;
                # Set the root to /usr/lib (inside this location this means that we are
                # giving access to the files under /usr/lib/cgi-bin)
                root  /home/cloud/bbb-balancer-resolver;
                # Fastcgi socket
                fastcgi_pass  unix:/var/run/fcgiwrap.socket;
                # Fastcgi parameters, include the standard ones                                                                                                                                                                                                                           
                include /etc/nginx/fastcgi_params;
                # Adjust non standard parameters (SCRIPT_FILENAME)
                fastcgi_param SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        }
[...]
```
