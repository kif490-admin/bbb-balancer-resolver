#include "common.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


int is_valid_uuid_v4(char *buffer) {
	if (buffer == NULL || strlen(buffer) != 36)
		return 0;

// 034d9d19-0182-4fd8-aa18-21ffd18bf956	
	int i;
	int counter = 0;
	for (i = 0; i < 8; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 4; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	if (buffer[counter++] != '-')
			return 0;
	for (i = 0; i < 12; i++) {
		if (!is_valid_hex(buffer[counter++]))
			return 0;
	}
	return 1;
}

int is_valid_hex(char c) {
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}
int hex_to_int(char c) {
	if (c >= '0' && c <= '9') {
		return c - '0';
	}
	c = tolower(c);
	if (c >= 'a' && c <= 'z') {
		return c - 'a';
	}
	return 0;
}

char int_to_hex(int x) {
	if (x >= 0 && x <= 9)
		return '0' + x;
	if (x >= 10 && x <= 15)
		return 'a' + x - 10;
	return '0';
}


int is_percent_encoded(char * str) {
	if (str == NULL)
		return 0;
	size_t len = strlen(str);
	int mode = 0; // 0: expecting unreserved char or percent // 1: expecting first hex  // 2: expecting second hex
	size_t i;
	char c;
	int wasfirsthexzero = 0;
	for (i = 0; i < len; i++) {
		c = str[i];
		switch (mode) {
		case 0:
			if (c == '%') {
				mode = 1;
				break;
			} else if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
				break;
			} else {
				return 0;
			}
		case 1:
			if (is_valid_hex(c)) {
				mode = 2;
				wasfirsthexzero = (c == '0');
				break;
			} else {
				return 0;
			}
		case 2:
			if (is_valid_hex(c)) {
				mode = 0;
				if (wasfirsthexzero && (c == '0'))
					return 0; // terminating null bytes are not allowed!!!
				break;
			} else {
				return 0;
			}
		}
	}

	return (mode == 0);
}


char * percent_decode(const char * str) {
	if (str == NULL)
		return NULL;

	char * res = malloc(sizeof(char) * (strlen(str) +1));
	if (res == NULL)
		return NULL;

	int r = 0;
	int w = 0;
	char c;
	char buf[3];
	buf[2] = '\0';
	while ((c = str[r]) != '\0') {
		if (c == '%') {
			buf[0] = str[r+1];	
			buf[1] = str[r+2];	
			res[w] = (char)(strtol(buf,NULL,16));
			r+=2;
		} else {
			res[w] = str[r];
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;
}

char * percent_encode(const char * str) {
	if (str == NULL)
		return NULL;
	size_t ressize = strlen(str) + 1;
	char * res = malloc(sizeof(char) * ressize);
	if (res == NULL)
		return NULL;

	int r = 0;
	int w = 0;
	char c;
	while ((c = str[r]) != '\0') {
		if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~') {
			res[w] = str[r];
		} else {
			ressize += 2;
			res = realloc(res, ressize);
			if (res == NULL)
				return NULL;
			res[w]   = '%';
			res[w+1] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0xf0)) >> 4) );
			res[w+2] = int_to_hex((unsigned char)(((unsigned char)(((unsigned char)c) & 0x0f))));
			//printf("percent_encode: c: '%c' (int): %d (hex): %X\n", c, c, c);
			w+=2;
		}
		w++;
		r++;
	}
	res[w] = '\0';
	return res;

}

