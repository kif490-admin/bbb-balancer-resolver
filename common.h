#ifndef COMMON_H_
#define COMMON_H_

#define UNUSED(x) (void)(x)


int is_valid_uuid_v4(char *buffer);

int is_valid_hex(char c);
int hex_to_int(char c);
char int_to_hex(int x);

int is_percent_encoded(char * str);
char * percent_decode(const char * str);
char * percent_encode(const char * str);

#endif
