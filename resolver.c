#include "common.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include <curl/curl.h>

#define QUERYSTRMAXLEN 256
#define BUFSIZENAME 101
#define BUFSIZEROOM 16


// Greenlight Base Url. You may want to update this.
#define BBBBASEURL "https://meet.gwdg.de"

enum querystrdfa_alphabet {
	querystrdfa_alphabet_n,
	querystrdfa_alphabet_a,
	querystrdfa_alphabet_m,
	querystrdfa_alphabet_e,
	querystrdfa_alphabet_r,
	querystrdfa_alphabet_o,
	querystrdfa_alphabet_null,
	querystrdfa_alphabet_and,
	querystrdfa_alphabet_eq,
	querystrdfa_alphabet_char,
	querystrdfa_alphabet_illegalchar
};
enum querystrdfa_state {
	querystrdfa_state_start,
	querystrdfa_state_end,
	querystrdfa_state_knn,
	querystrdfa_state_krr,
	querystrdfa_state_kne,
	querystrdfa_state_knm,
	querystrdfa_state_kna,
	querystrdfa_state_kro1,
	querystrdfa_state_kro2,
	querystrdfa_state_krm,
	querystrdfa_state_vname,
	querystrdfa_state_vroom,
	querystrdfa_state_error
};

static enum querystrdfa_state querystrdfa_transition(enum querystrdfa_state previousstate, enum querystrdfa_alphabet input);

void print_error(const char * errormsg) {
	printf("{ \"error\": \"%s\" }\n", errormsg);
}

size_t noop_cb(void *ptr, size_t size, size_t nmemb, void *data) {
	return size * nmemb;
}

int bbb_join_url_req2(const char * requrl, const char * bbburl) {
	/* based on code from:
	*     - https://curl.se/libcurl/c/http-post.html
	*     - https://curl.se/libcurl/c/httpcustomheader.html
	*     - https://curl.se/libcurl/c/getredirect.html
	*/


	CURL *curl;
	CURLcode res;
	char *location = NULL;
	long response_code;
	
	
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
	 	struct curl_slist *chunk = NULL;
	 
		/* Add custom headers */ 
		chunk = curl_slist_append(chunk, "Content-Type: application/x-www-form-urlencoded");
		chunk = curl_slist_append(chunk, "DNT: 1");
		chunk = curl_slist_append(chunk, "Cache-Control: no-cache");
		
		/* set our custom set of headers */ 
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		/* First set the URL that is about to receive our POST. This URL can
			just as well be a https:// URL if that is what should receive the
			data. */ 
		curl_easy_setopt(curl, CURLOPT_URL, requrl);
		
		// Disable writing to stdout
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, noop_cb);

		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK) {
			print_error("curl_easy_perform() failed");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			return 0;
		}


		res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		if((res == CURLE_OK) && ((response_code / 100) != 3)) {
			/* a redirect implies a 3xx response code */ 
			print_error("Didn't receive a redirect. Is the BBB Room setup correctly? Does it even exist?");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			return 0;
		}

		res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &location);
		if(!((res == CURLE_OK) && location)) {
			print_error("Didn't receive a location. Is the BBB Room setup correctly?");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			return 0;
		}

		/* This is the new absolute URL that you could redirect to, even if
		 * the Location: response header may have been a relative URL. */ 
		//printf("Redirected to: %s\n", location);
		printf("{ \"bbbjoinurl\": \"%s\", \"bbburl\": \"%s\" }\n", location, bbburl);
		
		/* always cleanup */ 
		curl_easy_cleanup(curl);
		curl_slist_free_all(chunk); /* free the custom headers */ 
	} else {
		return 0;
	}
	return 1;

}
int bbb_join_url(const char * name, const char * room) {
	/* based on code from:
	*     - https://curl.se/libcurl/c/http-post.html
	*     - https://curl.se/libcurl/c/httpcustomheader.html
	*     - https://curl.se/libcurl/c/getredirect.html
	*/



	// curl -i "${BASEURL}/b/${ROOMID}" -H "Content-Type: application/x-www-form-urlencoded" -H "DNT: 1" -H "Cache-Control: no-cache" --data-raw "utf8=%E2%9C%93&%2Fb%2F${ROOMID}%5Bsearch%5D=&%2Fb%2F${ROOMID}%5Bcolumn%5D=&%2Fb%2F${ROOMID}%5Bdirection%5D=&%2Fb%2F${ROOMID}%5Bjoin_name%5D=${ENUSERNAME}" > resp-02.html
	// -H "Content-Type: application/x-www-form-urlencoded" -H "DNT: 1" -H "Cache-Control: no-cache"
	// "utf8=%E2%9C%93&%2Fb%2F${ROOMID}%5Bsearch%5D=&%2Fb%2F${ROOMID}%5Bcolumn%5D=&%2Fb%2F${ROOMID}%5Bdirection%5D=&%2Fb%2F${ROOMID}%5Bjoin_name%5D=${ENUSERNAME}"

	char bufurl[128];
	char bufdata[256 + BUFSIZENAME + 4 * BUFSIZEROOM];
	bufurl[0] = '\0';
	bufdata[0] = '\0';

	sprintf(bufurl, "%s/b/%s", BBBBASEURL, room);
	sprintf(bufdata, "utf8=%%E2%%9C%%93&%%2Fb%%2F%s%%5Bsearch%%5D=&%%2Fb%%2F%s%%5Bcolumn%%5D=&%%2Fb%%2F%s%%5Bdirection%%5D=&%%2Fb%%2F%s%%5Bjoin_name%%5D=%s", room, room, room, room, name);


	CURL *curl;
	CURLcode res;
	char *location = NULL;
	long response_code;

	int retres = 0;
	
	/* In windows, this will init the winsock stuff */ 
	curl_global_init(CURL_GLOBAL_ALL);
	
	/* get a curl handle */ 
	curl = curl_easy_init();
	if(curl) {
	 	struct curl_slist *chunk = NULL;
	 
		/* Add custom headers */ 
		chunk = curl_slist_append(chunk, "Content-Type: application/x-www-form-urlencoded");
		chunk = curl_slist_append(chunk, "DNT: 1");
		chunk = curl_slist_append(chunk, "Cache-Control: no-cache");
		
		/* set our custom set of headers */ 
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);

		/* First set the URL that is about to receive our POST. This URL can
			just as well be a https:// URL if that is what should receive the
			data. */ 
		curl_easy_setopt(curl, CURLOPT_URL, bufurl);
		/* Now specify the POST data */ 
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, bufdata);
		
		// Disable writing to stdout
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, noop_cb);

		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK) {
			print_error("curl_easy_perform() failed");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			curl_global_cleanup();
			return 0;
		}


		res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
		if((res == CURLE_OK) && ((response_code / 100) != 3)) {
			/* a redirect implies a 3xx response code */ 
			print_error("Didn't receive a redirect. Is the BBB Room setup correctly? Does it even exist?");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			curl_global_cleanup();
			return 0;
		}

		res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &location);
		if(!((res == CURLE_OK) && location)) {
			print_error("Didn't receive a location. Is the BBB Room setup correctly?");
			curl_easy_cleanup(curl);
			curl_slist_free_all(chunk); /* free the custom headers */ 
			curl_global_cleanup();
			return 0;
		}

		/* This is the new absolute URL that you could redirect to, even if
		 * the Location: response header may have been a relative URL. */ 
		//printf("Redirected to: %s\n", location);
		//printf("{ \"bbbjoinurl\": \"%s\", \"bbburl\": \"%s\" }\n", location, bufurl);

		retres = bbb_join_url_req2(location, bufurl);
		
		/* always cleanup */ 
		curl_easy_cleanup(curl);
		curl_slist_free_all(chunk); /* free the custom headers */ 
	} else {
		curl_global_cleanup();
		return retres;
	}
	curl_global_cleanup();
	return retres;
}

int is_valid_bbb_roomid_char(char c) {
	return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || c == '-');

}
int is_valid_bbb_roomid(const char * room) {
	if (room == NULL)
		return 0;

	// strlen 11: 3 Sections: exa-a1b-cd2
	// strlen 15: 4 Sections: exa-a1b-cd2-3ef
	if (!(strlen(room) == 11 || strlen(room) == 15))
		return 0;

	size_t i = 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (room[i++] != '-') return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (room[i++] != '-') return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	if (!is_valid_bbb_roomid_char(room[i++])) return 0;

	if (strlen(room) == 15) { // 4 sections
		if (room[i++] != '-') return 0;
		if (!is_valid_bbb_roomid_char(room[i++])) return 0;
		if (!is_valid_bbb_roomid_char(room[i++])) return 0;
		if (!is_valid_bbb_roomid_char(room[i++])) return 0;
	}

	return 1;
}



int parse_querystr(const char * querystr, char * name, char * room) {
	if (querystr == NULL)
		return 0;
	if (strlen(querystr) > QUERYSTRMAXLEN)
		return 0;

	size_t namelen = 0;
	size_t roomlen = 0;
	name[namelen] = '\0';
	room[roomlen] = '\0';
	int detectedname = 0;
	int detectedroom = 0;

	enum querystrdfa_state curstate = querystrdfa_state_start;
	enum querystrdfa_state prevstate = curstate;

	size_t i = 0;
	char c;
	enum querystrdfa_alphabet inp;

	while (curstate != querystrdfa_state_error && curstate != querystrdfa_state_end) {
		c = querystr[i];
		// convert c to alphabet
		if (c == 'n')
			inp = querystrdfa_alphabet_n;
		else if (c == 'a')
			inp = querystrdfa_alphabet_a;
		else if (c == 'm')
			inp = querystrdfa_alphabet_m;
		else if (c == 'e')
			inp = querystrdfa_alphabet_e;
		else if (c == 'r')
			inp = querystrdfa_alphabet_r;
		else if (c == 'o')
			inp = querystrdfa_alphabet_o;
		else if (c == '\0')
			inp = querystrdfa_alphabet_null;
		else if (c == '&')
			inp = querystrdfa_alphabet_and;
		else if (c == '=')
			inp = querystrdfa_alphabet_eq;
		else if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' || c == '.' || c == '_' || c == '~' || c == '%')
			inp = querystrdfa_alphabet_char;
		else
			inp = querystrdfa_alphabet_illegalchar;
	
		// update state
		prevstate = curstate;
		curstate = querystrdfa_transition(prevstate, inp);
	
		// detect state changes
		if (curstate != prevstate) {
			
			if (curstate == querystrdfa_state_vname) {
				if (detectedname == 1) {
					return 0;
				}
				detectedname = 1;
			} else if (curstate == querystrdfa_state_vroom) {
				if (detectedroom == 1) {
					return 0;
				}
				detectedroom = 1;
			}




		}

		// read values
		if (curstate == querystrdfa_state_vname && prevstate == querystrdfa_state_vname) {
			name[namelen] = c;
			namelen++;
			if (namelen >= BUFSIZENAME)
				return 0;
			name[namelen] = '\0';
		} else if (curstate == querystrdfa_state_vroom && prevstate == querystrdfa_state_vroom) {
			room[roomlen] = c;
			roomlen++;
			if (roomlen >= BUFSIZEROOM)
				return 0;
			room[roomlen] = '\0';
		}

		i++;
	}

	if (curstate != querystrdfa_state_end)
		return 0;

	// final checks
	if (detectedname == 0 || detectedroom == 0)
		return 0;
	if (namelen < 1 || roomlen < 1)
		return 0;


	if (!is_percent_encoded(name))
		return 0;
	if (!is_valid_bbb_roomid(room))
		return 0;


	return 1;

}

int main(int argc, char ** argv) {

	//printf("Content-type: text/html\n\n");
	printf("Cache-Control: no-store\n");
	printf("Access-Control-Allow-Origin: *\n");
	printf("Content-type: application/json\n\n");

	char * querystr = NULL;
	char name[BUFSIZENAME + 1];
	char room[BUFSIZEROOM + 1];

	if ((querystr = getenv("QUERY_STRING")) == NULL) {
		//printf("ERROR: QUERY_STRING not found. Is this even a server?<br />\n");
		print_error("QUERY_STRING not found. Is this even a server?");
		/*if (argc == 2)
			querystr = argv[1];
		else*/
			return 1;
	}

	if (!parse_querystr(querystr, name, room)) {
		//printf("Failed to parse querystr!\n");
		print_error("Failed to parse the query string! Invalid characters or perhaps the name is too long? Or maybe just missing some data?");
		return 1;
	}

	/*
	printf("QUERY_STRING: \"%s\"<br />\n", querystr);
	printf("name[%ld]: \"%s\"<br />\n", strlen(name), name);
	printf("room[%ld]: \"%s\"<br />\n", strlen(room), room);*/

	if (!bbb_join_url(name, room)) {
		//printf("Failed to get url!\n");
		return 1;
	}

	return 0;
}

static enum querystrdfa_state querystrdfa_transition(enum querystrdfa_state previousstate, enum querystrdfa_alphabet input) {
	/* This code was generated using [dfa-transition-table-to-c-code](https://gitlab.gwdg.de/j.vondoemming/dfa-transition-table-to-c-code).*/
	/* original input: 
	* δ	n	a	m	e	r	o	null	and	eq	char	illegalchar
	* start	knn	error	error	error	krr	error	error	error	error	error	error
	* end	error	error	error	error	error	error	error	error	error	error	error
	* knn	error	kna	error	error	error	error	error	error	error	error	error
	* krr	error	error	error	error	error	kro1	error	error	error	error	error
	* kne	error	error	error	error	error	error	error	error	vname	error	error
	* knm	error	error	error	kne	error	error	error	error	error	error	error
	* kna	error	error	knm	error	error	error	error	error	error	error	error
	* kro1	error	error	error	error	error	kro2	error	error	error	error	error
	* kro2	error	error	krm	error	error	error	error	error	error	error	error
	* krm	error	error	error	error	error	error	error	error	vroom	error	error
	* vname	vname	vname	vname	vname	vname	vname	end	start	error	vname	error
	* vroom	vroom	vroom	vroom	vroom	vroom	vroom	end	start	error	vroom	error
	* error	error	error	error	error	error	error	error	error	error	error	error
	*/
	const static enum querystrdfa_state statetransitiontable[13][11] = {
		/* start: */ { querystrdfa_state_knn, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_krr, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* end: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* knn: */ { querystrdfa_state_error, querystrdfa_state_kna, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* krr: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_kro1, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* kne: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_vname, querystrdfa_state_error, querystrdfa_state_error },
		/* knm: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_kne, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* kna: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_knm, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* kro1: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_kro2, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* kro2: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_krm, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error },
		/* krm: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_vroom, querystrdfa_state_error, querystrdfa_state_error },
		/* vname: */ { querystrdfa_state_vname, querystrdfa_state_vname, querystrdfa_state_vname, querystrdfa_state_vname, querystrdfa_state_vname, querystrdfa_state_vname, querystrdfa_state_end, querystrdfa_state_start, querystrdfa_state_error, querystrdfa_state_vname, querystrdfa_state_error },
		/* vroom: */ { querystrdfa_state_vroom, querystrdfa_state_vroom, querystrdfa_state_vroom, querystrdfa_state_vroom, querystrdfa_state_vroom, querystrdfa_state_vroom, querystrdfa_state_end, querystrdfa_state_start, querystrdfa_state_error, querystrdfa_state_vroom, querystrdfa_state_error },
		/* error: */ { querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error, querystrdfa_state_error }
	};

	enum querystrdfa_state res = statetransitiontable[previousstate][input];

	if (res != previousstate) {
		/* you may add code for transition state changes here */
	}
	return res;
}
